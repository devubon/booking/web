import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { LayoutService } from 'src/app/layout/service/app.layout.service';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { NewPasswordService } from './newpassword.service';
import { Message, MessageService } from 'primeng/api';


@Component({
	templateUrl: './newpassword.component.html',
    providers: [MessageService]
})
export class NewPasswordComponent {
    public validateForm!: FormGroup;
	constructor(
        private layoutService: LayoutService,
        private newpasswordService : NewPasswordService,
        private messageService: MessageService,
        private router: Router) {}

	get filledInput(): boolean {
		return this.layoutService.config.inputStyle === 'filled';
	}
    msgs: Message[] = [];
    messages: any | undefined;

    password:any;
    passwordConfirm:any;
    username:any;
    cid:any;
    userID = '';

    async checkUsername(data: any) {
        // console.log('user:',this.username);

        if (this.username != '' && this.username != null && this.username != undefined) {
            const res: any = await this.newpasswordService.checkUsername(this.username);
            // console.log('res:',res);

            if (res.data.ok) {
                this.userID = res.data.results
                // console.log('userID:',this.userID);
                this.showSuccessViaToast('success','Success','มีชื่อในระบบ');
                this.messages = [];
            } else {
                this.showErrorViaMessages('error','Error','ไม่พบ Username นี้'  );
            }
        }
    }

    async resetPassword(data: any){
        if(this.userID != ''){
            if(this.password == this.passwordConfirm){
                const userData = {
                    password: this.password
                };
                // console.log('user:', userData,this.cid,this.userID);
                try {
                    let res: any = await this.newpasswordService.resetPassword(this.userID,this.cid,userData);
                    // console.log('resetpass:',res);
                    if(res.data.results.length > 0){
                        this.showSuccessViaToast('success','Success','บันทึกสำเร็จ');
                        this.messages = [];
                        this.router.navigate(['/']);
                    }else{
                        // console.log('cid:',this.userID);
                        this.showErrorViaMessages('error','Error','เลขบัตรประจำตัวประชาชน 4 หลักหลังไม่ถูกต้อง'  );
                    }
                } catch (error) {
                    console.log(error);
                    this.showErrorViaMessages('error','Error','เลขบัตรประจำตัวประชาชน 4 หลักหลังไม่ถูกต้อง'  );
                }

            }
        } else {
            this.showErrorViaMessages('error','Error','กรุณาตรวจสอบ Username'  );
        }

    }

    showErrorViaToast(severity: any, summary: any, detail: any) {
        this.messageService.add({  severity: severity, summary:summary, detail: detail  });
    }

    showSuccessViaToast(severity: any, summary: any, detail: any) {
        this.messageService.add({  severity: severity, summary:summary, detail: detail });
    }

    showErrorViaMessages(severity: any, summary: any, detail: any) {
        this.messages = [
            { severity: severity, summary: summary, detail: detail },
        ];
    }

    showSuccessViaMessages(severity: any, summary: any, detail: any) {
        this.messages = [
            { severity: severity, summary: summary, detail: detail },
        ];
    }
}
