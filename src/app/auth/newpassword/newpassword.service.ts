import { Injectable } from '@angular/core';
import axios from 'axios';
import { environment } from '../../../environments/environment';

@Injectable({
    providedIn: 'root',
})
export class NewPasswordService {
    // pathPrefix: any = `:40014`
    // pathPrefixAuth: any = `:40001`;

    private axiosInstance = axios.create({
        baseURL: `${environment.apiUrl}`,
    });

    constructor() {
        this.axiosInstance.interceptors.request.use((config) => {
            const token = sessionStorage.getItem('token');
            if (token) {
                config.headers['Authorization'] = `Bearer ${token}`;
            }
            return config;
        });
    }

    async save(data: any) {
        const url = `/register/user`;
        return this.axiosInstance.post(url, data);
    }
    async resetPassword(id:any,cid:any,data: any) {
        // console.log(data);

        const url = `/register/resetPassword/${id}/${cid}`;
        return this.axiosInstance.put(url, data);
    }

    async saveProfiles(data: any) {
        const url = `/register/profile`;
        return this.axiosInstance.post(url, data);
    }

    async login(username: any, password: any) {
        const url = `/login`;
        return this.axiosInstance.post(url, {
          username, password
        });
    }

    async getUserInfo() {
        const url = `/profile/info`;
        return this.axiosInstance.get(url);
    }

    async checkUsername(username: any) {
        const url = `/checkUser/getByUsername/`+ username;
        return this.axiosInstance.get(url);
    }

}
