import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CardModule } from 'primeng/card';
import { TimelineModule } from 'primeng/timeline';
import { AvatarModule } from 'primeng/avatar';
import { AvatarGroupModule } from 'primeng/avatargroup';
import { BlockUIModule } from 'primeng/blockui';
import { TableModule } from 'primeng/table';
import { TagModule } from 'primeng/tag';
import { CheckboxModule } from 'primeng/checkbox';
import { FormsModule } from '@angular/forms';
import { ButtonModule } from 'primeng/button';
import {DividerModule} from 'primeng/divider';
import {BadgeModule} from 'primeng/badge';

import { AppointCardRoutingModule } from './appoint-card-routing.module';
import { AppointCardComponent } from './appoint-card.component';

@NgModule({
  declarations: [
    AppointCardComponent
  ],
  imports: [
    CommonModule,
    CardModule,
    TimelineModule,
    AvatarGroupModule,
    AvatarModule,
    BlockUIModule,
    TableModule,
    TagModule,
    CheckboxModule,
    FormsModule,
    ButtonModule,
    AppointCardRoutingModule,
    DividerModule,
    BadgeModule
  ]
})

export class AppointCardModule { }