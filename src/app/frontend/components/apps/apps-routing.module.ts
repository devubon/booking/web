import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

@NgModule({
    imports: [
        RouterModule.forChild([
            { path: '', pathMatch: 'full', redirectTo: 'list-booking' },

            {
                path: 'activity',
                data: { breadcrumb: 'เลือกบริการ' },
                loadChildren: () =>
                    import('./activities/activities.module').then(
                        (m) => m.ActivitiesModule
                    ),
            },
            {
                path: 'hospitals',
                data: { breadcrumb: 'เลือกโรงพยาบาล' },
                loadChildren: () =>
                    import('./hospitals/hospital.module').then(
                        (m) => m.HospitalModule
                    ),
            },
            {
                path: 'calendar',
                data: { breadcrumb: 'เลือกวันรับบริการ' },
                loadChildren: () =>
                    import('./calendar/calendar.app.module').then(
                        (m) => m.CalendarAppModule
                    ),
            },
            {
                path: 'list-booking',
                data: { breadcrumb: 'Home' },
                loadChildren: () =>
                    import('./list-booking/list-booking.module').then(
                        (m) => m.ListBookingModule
                    ),
            },
            {
                path: 'appoint-card',
                data: { breadcrumb: 'บัตรจอง' },
                loadChildren: () =>
                    import('./appoint-card/appoint-card.module').then(
                        (m) => m.AppointCardModule
                    ),
            }
            // { path: '', data: { breadcrumb: 'Home' }, loadChildren: () => import('./hospitals/hospital.module').then(m => m.HospitalModule) },

            // { path: '**', redirectTo: '/notfound' }
        ]),
    ],
    exports: [RouterModule],
})
export class AppsRoutingModule {}
