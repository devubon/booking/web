import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CardModule } from 'primeng/card';
import { TimelineModule } from 'primeng/timeline';
import { AvatarModule } from 'primeng/avatar';
import { AvatarGroupModule } from 'primeng/avatargroup';
import { ListBookingRoutingModule } from './list-booking-routing.module';
import {ListBookingComponent} from './list-booking.component'
import { ButtonModule } from 'primeng/button';
import { FieldsetModule } from 'primeng/fieldset';


@NgModule({
  declarations: [ListBookingComponent],
  imports: [
    CommonModule,
    ListBookingRoutingModule,CardModule,TimelineModule,AvatarGroupModule,AvatarModule,ButtonModule,FieldsetModule
  ]
})
export class ListBookingModule { }
