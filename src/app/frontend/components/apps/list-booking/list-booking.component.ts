import { Component, OnInit } from '@angular/core';
import { PrimeIcons } from 'primeng/api';
import { ListBookingService } from './list-booking.service';
import { LookupService} from '../../../../backend/services/lookup.service';
import { Router } from '@angular/router';


@Component({
  selector: 'app-list-booking',
  templateUrl: './list-booking.component.html',
  styleUrls: ['./list-booking.component.scss']
})
export class ListBookingComponent implements OnInit {
//ประกาศตัวเปร
  userId = sessionStorage.getItem('userId');
  events1: any[] = [];
  events: any[] = [];
  transformedData: any[] = [];
  listBooks:any; 
  listHospitalInfo:any = [];
  listService:any = [];
  constructor(
    private ListBookingService: ListBookingService,
    private lookupService:LookupService,
    private router:Router
    ) 
    {
    }
     async ngOnInit() {
     await this.getHospital();
     await this.getService();
     await this.getData();
  }
  async getHospital() {
    const res:any = await this.lookupService.listHospitalInfo();
    let hospital = [];
    hospital = res.data;
    if(hospital.ok){
        this.listHospitalInfo = hospital.results;
    } else {
        // alert('error load period')
    }

}
  async getService() {
    const res:any = await this.lookupService.listService();
    let service = [];
    service = res.data;
    if(service.ok){
        this.listService = service.results;
    } else {
        // alert('error load period')
    }

  }
navigateToOtherPage() {
  this.router.navigate(['/frontend/apps/activity']);
}
thaiDateFormat(date:any){
  const thaiMonth = [
      'ม.ค.','ก.พ.','มี.ค.','เม.ย.','พ.ค.','มิ.ย.','ก.ค.','ส.ค.','ก.ย.','ต.ค.','พ.ย.','ธ.ค.'
  ]
  const newDate = new Date(date);
  let day = newDate.getDate();
  let monthName = thaiMonth[newDate.getMonth()];
  let thaiYear = newDate.getFullYear() + 543;

  let thaidate = day + ' ' + monthName + ' ' + thaiYear;
  return thaidate;
  
}
  async transformedDatalist(listbooks:any){

    if (listbooks.results.length > 0) { 
        // ไม่แสดงใบนัดที่ยกเลิก
        listbooks = listbooks.results.filter((x: any) => x.status != "cancel");

        for (let v of listbooks) {

          let h: any = this.listHospitalInfo.find(
            (x: any) => x.hospital_id === v.hospital_id
          );
          v.hospital_name = h.hospital_name;

          let sv: any = this.listService.find(
            (x: any) => x.service_id === v.service_id
          );
          v.service_name = sv.service_name;

          v.is_confirm = v.is_confirm ? 'ยืนยันการจอง' : 'รอยืนยัน' ;
      }
      
      this.events = listbooks;   

  } else {
      // alert('Error loading clinics');
  }
  }
  async getData() {
    // this.blockedPanel = true;
    
    try{
        const res :any  = await this.ListBookingService.list(this.userId);
        
        let listbooks = [];
        listbooks = res.data; 
        this.transformedDatalist(listbooks);  
           
    }catch(error) {
        console.log(error);
    }
  }

  appointCard(id:number){

    sessionStorage.setItem('reserveId',id.toString());
    this.router.navigate(['/frontend/apps/appoint-card']);
  }

}
