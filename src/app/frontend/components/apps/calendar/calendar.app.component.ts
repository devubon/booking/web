import { Component, OnInit,ElementRef } from '@angular/core';
import { SlotService } from './calendar.service';
import { DateTime } from 'luxon';
import { JwtHelperService } from '@auth0/angular-jwt';
import { Router } from '@angular/router';

import { BlockUI } from 'primeng/blockui';
// @fullcalendar plugins
import dayGridPlugin from '@fullcalendar/daygrid';
import interactionPlugin from '@fullcalendar/interaction';
import timeGridPlugin from '@fullcalendar/timegrid';
import { CalendarOptions } from '@fullcalendar/core';

import { LookupService } from '../../../service/lookup.service';
import * as _ from 'lodash';
import { ex } from '@fullcalendar/core/internal-common';

// 0830758132  111111
@Component({
    templateUrl: './calendar.app.component.html',
    styleUrls: ['./calendar.app.component.scss'],
})
export class CalendarAppComponent implements OnInit {
    jwtHelper: JwtHelperService = new JwtHelperService();
    events: any[] = [];

    today: string = '';

    calendarOptions: any;

    showDialog: boolean = false;

    clickedEvent: any = null;

    dateClicked: boolean = false;

    edit: boolean = false;

    tags: any[] = [];

    view: string = '';

    changedEvent: any;
    selectedSlotID: any;

    now = DateTime.now();

    selectServiceTypeId: Number;
    listSlot: any = [];
    listSlotByID: any = [];
    listHospitals: any = [];
    listPeriods: any = [];
    listCustomers: any = [];
    selectHospitals: any = [];
    selectDate: any;
    listAvialableSlot: any = [];
    listAvialableHospital: any = [];

    isLoading: boolean = false;

    customer: any = {};
    customerName: string = '';
    customerTel: string = '';
    customerLineId: string = '';
    customerAge: string = '';
    customerCID: string = '';
    customerPassport: string = '';
    customerChronics: string = '';
    customerChronicsBtnRadio: string = '';
    customerId: string = '';

    userId: Number;
    serviceTypeId: any;
    selectSlot: any;

    is_ban: boolean = false;
    banExpire: any;

    messages: any | undefined;
    txtChronic: string = '';

    constructor(
        private slotService: SlotService,
        private lookupService: LookupService,
        private router: Router,
        private el: ElementRef
    ) {
        this.selectServiceTypeId = Number(
            sessionStorage.getItem('serviceTypeId')
        );
        this.serviceTypeId = Number(sessionStorage.getItem('serviceTypeId'));
        this.userId = Number(sessionStorage.getItem('userId'));

        this.listSlot = JSON.parse(sessionStorage.getItem('slots') || '[]');
    }

    ngOnInit(): void {
        this.isLoading = true;
        this.today = this.now.setLocale('th-TH').toISODate();

        this._prepareCalendar();
        this.getCustomer();
    }

    async getCustomer() {
        this.isLoading = false;

        const userId: any = sessionStorage.getItem('userId');
        let _profile: any = await this.lookupService.getProfilesById(userId);
        
        if (_profile.data.results.length > 0) {
            const res: any = await this.lookupService.listCustomerByCID(
                _profile.data.results[0].cid
            );
            
            if (res.data.results.length == 0) {
                let profile = _profile.data.results[0];
                
                this.customerName = profile.title + ' ' + profile.fullname;
                this.customerTel = profile.phone_number;
                this.customerCID = profile.cid;
                this.listCustomers = {
                    customer_name: this.customerName,
                    phone_number: this.customerTel,
                    cid: this.customerCID,
                };

                if(this.listCustomers.chronic == ""){
                    this.customerChronicsBtnRadio = 'false';
                }else{
                    this.customerChronicsBtnRadio = 'true';
                }

            } else {
                this.listCustomers = res.data.results[0];
                this.customerName = this.listCustomers.customer_name;
                this.customerTel = this.listCustomers.phone_number;
                this.customerLineId = this.listCustomers.line_id;
                this.customerAge = this.listCustomers.age;
                this.customerCID = this.listCustomers.cid;
                this.customerPassport = this.listCustomers.passport;
                this.customerChronics = this.listCustomers.chronic;
                this.banExpire = this.listCustomers.expire_bane_date;
                this.customerId = this.listCustomers.customer_id;
                
                if(this.listCustomers.chronic == ""){
                    this.customerChronicsBtnRadio = 'false';
                }else{
                    this.customerChronicsBtnRadio = 'true';
                }
                
            }
            let customer = [];

            customer = res.data.results;
            
            if (customer.ok) {
                this.listCustomers = customer.results[0];
                this.isLoading = false;

            } else {
                // alert('error load period')
            }
        }
    }

    async getHospital() {
        const res: any = await this.lookupService.listHospital();
        let hospital = [];
        hospital = res.data;
        if (hospital.ok) {
            this.listHospitals = hospital.results;
        } else {
            // alert('error load period');
        }
    }

    async getByServiceID(hospital: any) {
        const hospital_id = hospital.hospital_id;
        let _slot = this.listSlot.filter(
            (event: any) =>
                event.hospital_id == hospital_id &&
                DateTime.fromISO(event.slot_date)
                    .setLocale('th-TH')
                    .toISODate() == this.selectDate
        );
        for (let v of _slot) {
            v.valuename =
                v.slot_name +
                ' วันที่ ' +
                this.thaiDateFormat(v.slot_date) +
                ' (' +
                v.period_name +
                'น.)';
        }
        this.listSlotByID = _slot;
    }

    async slotchange(slotData: any) {
        this.selectedSlotID = slotData.slot_id;
    }

    thaiDateFormat(date: any) {
        const thaiMonth = [
            'ม.ค.',
            'ก.พ.',
            'มี.ค.',
            'เม.ย.',
            'พ.ค.',
            'มิ.ย.',
            'ก.ค.',
            'ส.ค.',
            'ก.ย.',
            'ต.ค.',
            'พ.ย.',
            'ธ.ค.',
        ];
        const newDate = new Date(date);
        let day = newDate.getDate();
        let monthName = thaiMonth[newDate.getMonth()];
        let thaiYear = newDate.getFullYear() + 543;

        let thaidate = day + ' ' + monthName + ' ' + thaiYear;
        return thaidate;
    }

    _prepareHospitalX() {
        this.listHospitals = Array.from(
            new Set(this.listSlot.map((event: any) => event.hospital_id))
        ).map((hospital_id: any) => {
            return this.listSlot.find(
                (event: any) => event.hospital_id === hospital_id
            );
        });
    }

    selectHospital(hospital_id: any) {
        this.listPeriods = this.listSlot.filter(
            (event: any) => event.hospital_id === hospital_id
        );
    }

    async _prepareCalendar() {

        let events = await this.listSlot.map((event: any) => ({
            // title: event.slot_name,
            start: DateTime.fromISO(event.slot_date)
                .setLocale('th-TH')
                .toISODate(),
            display: 'background',  
            backgroundColor:'transparent',          
           
            id: event.slot_id,
        }));


        this.calendarOptions = {
            plugins: [dayGridPlugin, timeGridPlugin, interactionPlugin],
            height: 320,
            initialView: 'dayGridMonth',
            initialDate: this.today,
            headerToolbar: {
                left: 'prev',
                center: 'title',
                right: 'next',
            },
            locale: 'th-TH',
            editable: true,
            selectable: true,
            selectMirror: true,
            dayMaxEvents: true,
            events: events,
            selectLongPressDelay: 100, //ค่าเร่ิมต้น สำหรับ teblet mobile  = 1000ms  คือต้องกดค้าง 1000 ms
            // eventClick: (e: MouseEvent) => this.onEventClick(e), //เมื่อคลิกเลือก event
            select: (e: MouseEvent) => this.onDateSelect(e), //เมื่อเลือกวันที่
            // dayRender: (arg: any) => this.handleDayRender(arg), // disable day if no event
       
            eventDidMount :this.eventDidMount ,         
           
          
        };
    }
    eventDidMount(info: any) {
       
        let dayEl = info.el.closest('.fc-daygrid-day');
        // console.log('Day Element:', dayEl.parents);
        // let parent = dayEl.parentNode;

        if (dayEl) {
            // Remove any existing background color
            dayEl.style.backgroundColor = '';           
            dayEl.style.backgroundColor = '#5bc36c'; // Set your desired background color
         
        } else {
            // console.log('Day Element not found');
        }
    }
    dayHeaderContent(arg: any) {

        const dayNumberElement = arg.dayNumberText;    
        // Set the color of the day number
        dayNumberElement.style.color = 'red'; // Set your desired color
    }    
   


    onEventClick(e: any) {
        this.clickedEvent = e.event;
        // console.log('click:', this.clickedEvent);
        let plainEvent = e.event.toPlainObject({
            collapseExtendedProps: true,
            collapseColor: true,
        });
        this.view = 'display';
        this.showDialog = true;

        this.changedEvent = { ...plainEvent, ...this.clickedEvent };
        this.changedEvent.start = this.clickedEvent.start;
        this.changedEvent.end = this.clickedEvent.end
            ? this.clickedEvent.end
            : this.clickedEvent.start;
    }

    onDateSelect(e: any) {
        this.view = 'new';
        this.getCustomer();

        this.selectDate = e.startStr; // set date
        this.listAvialableSlot = []; // clear list slot
        // filter slot by date
        this.listAvialableSlot = this.listSlot.filter(
            (event: any) =>
                DateTime.fromISO(event.slot_date)
                    .setLocale('th-TH')
                    .toISODate() == e.startStr
        );
        // map hospital from slot by slot date
        let _hospital = Array.from(
            new Set(
                this.listAvialableSlot.map((event: any) => event.hospital_id)
            )
        ).map((hospital_id: any) => {
            return this.listAvialableSlot.find(
                (event: any) => event.hospital_id === hospital_id
            );
        });

        // clear list hospital
        this.listAvialableHospital = [];
        for (let v of _hospital) {
            let data = {
                hospital_id: v.hospital_id,
                hospital_name: v.hospital_name,
            };
            this.listAvialableHospital.push(data);
        }

        this.changedEvent = {
            ...e,
            title: null,
            description: null,
            location: null,
            backgroundColor: null,
            borderColor: null,
            textColor: null,
            tag: { color: null, name: null },
        };

        // check if slot is empty
        if (this.listAvialableSlot.length > 0) {
            this.showDialog = true;
        }
    }

    handleSave() {

        if(this.customerCID.length != 13){
            // this.isValidateUsername = false;
            this.showMessages('error', 'Error', 'เลขบัตรประชาชนไม่ถูกต้อง');
            return ;
        }

        if(this.customerName == ''){
            // this.isValidateUsername = false;
            this.showMessages('error', 'Error', 'โปรดระบุ ชื่อ-สกุล ผู้ขอรับบริการ');
            return ;
        }

        if(this.customerAge == ''){
            // this.isValidateUsername = false;
            this.showMessages('error', 'Error', 'โปรดระบุอายุผู้ขอรับบริการ');
            return ;
        }

        if(this.customerTel.length != 10){
            // this.isValidateUsername = false;
            this.showMessages('error', 'Error', 'เบอร์โทรศัพท์ไม่ถูกต้อง');
            return ;
        }

        if(this.customerChronicsBtnRadio == '' && this.customerChronics ==''){
            // this.isValidateUsername = false;
            this.showMessages('error', 'Error', 'โปรดระบุโรคประจำตัว/แพ้ยา/อื่นๆ');
            return ;
        }
        
        if(this.selectHospitals  == '' || this.selectHospitals  == undefined){
            // this.isValidateUsername = false;
            this.showMessages('error', 'Error', 'โปรดเลือกหน่วยบริการ');
            return ;
        }

        if(this.selectSlot  == '' || this.selectSlot  == undefined){
            // this.isValidateUsername = false;
            this.showMessages('error', 'Error', 'โปรดเลือกช่วงเวลา');
            return ;
        }

        if(this.customerChronicsBtnRadio == 'true' && this.customerChronics == ''){
            this.txtChronic  = "มีโรคประจำตัว (ผู้รับบริการไม่ระบุ)" ;

        }else if(this.customerChronicsBtnRadio == 'true' && this.customerChronics != ''){
            this.txtChronic = this.customerChronics;

        }else{
            this.txtChronic = '';
        }

            let data = {
                customer_name: this.customerName,
                phone_number: this.customerTel,
                line_id: this.customerLineId,
                age: this.customerAge,
                cid: this.customerCID,
                passport: this.customerPassport,
                chronic: this.txtChronic,
                customer_status: true,
                register_date: new Date(),
            };

            this.customer = data;

            this.save(this.customer);
    }

    onEditClick() {
        this.view = 'edit';
    }

    delete() {
        this.events = this.events.filter(
            (i) => i.id.toString() !== this.clickedEvent.id.toString()
        );
        this.calendarOptions = {
            ...this.calendarOptions,
            ...{ events: this.events },
        };
        this.showDialog = false;
    }

    validate() {
 

        if (
            this.selectHospitals &&
            this.selectSlot &&
            this.customerName &&
            this.customerTel &&
            this.customerCID &&
            this.customerAge
        ) {
            return true;
        } else {
            return false;
        }
    }

    async save(customer: any) {
        const check = await this.lookupService.listCustomerByCID(customer.cid);

        let customerId: number = 0;
        let expireBanDate: any;
        if (check.data.results.length == 0) {
            try {
                const res: any = await this.slotService.saveCustomer(customer);
                let data = res.data;
                // console.log(data);
                if (data.results.length > 0) {
                    customerId = data.results[0].customer_id;
                    expireBanDate = data.results[0].expire_bane_date;
                }
                // console.log('save',res);
                
            } catch (error) {
                console.log(error);
            }
        }
        else {
            // แก้ไขข้อมูล customer
            try{

                let body = {
                    customer_name: this.customerName,
                    phone_number: this.customerTel,
                    line_id: this.customerLineId,
                    age: this.customerAge,
                    passport: this.customerPassport,
                    chronic: this.customerChronics
                };

                const res: any = await this.slotService.editCustomer(this.customerId ,body);
                let data = res.data;      
                    // console.log(data);
                    
                if (data.results.length > 0) {
                    customerId = check.data.results[0].customer_id;
                    expireBanDate = check.data.results[0].expire_bane_date;
                }
                // console.log('edit',res);


            }catch (error){
                console.log(error);
            }
            
        }

        // check ban
        if (expireBanDate) {
            const currentDate = new Date();
            const expireDate = new Date(expireBanDate);
            if (currentDate > expireDate) {
                this.is_ban = false;
            } else {
                this.is_ban = true;
            }
            this.is_ban = true;
        } else {
            this.is_ban = false;
        }

        // save reserve
        if (this.is_ban) {
            this.showMessages(
                'error',
                'Error',
                'CID ผู้จองนัด ถูกระงับการใช้งาน เนื่องจากผู้ป่วยผิดนัด 3 ครั้ง'
            );
            // alert('CID ผู้จองนัด ถูกระงับการใช้งาน เนื่องจากผู้ป่วยผิดนัด 3 ครั้ง');
            return;
        } else {
            this.saveReserves(customerId);
        }
    }

    async saveReserves(customerId: any) {
        let reserve = {
            customer_id: customerId,
            slot_id: this.selectedSlotID,
            user_id: this.userId,
            service_type_id: this.selectServiceTypeId,
            reserve_date: this.today,
        };

        // console.log(reserve);

        try {
            const check: any = await this.lookupService.getSloteByCustomerID(
                customerId
            );
        // console.log(check);

            if (check.data.results.length > 0) {
                this.showMessages(
                    'error',
                    'Error',
                    'ไม่สามารถจองคิวซ้ำได้ คุณมีคิวเดิมอยู่แล้ว'
                );
                return;               
            } else {
                
                const res: any = await this.slotService.saveReserves(reserve);
                let data = res.data;
                
                if (data.results.length > 0) {
                    sessionStorage.setItem('reserveId',data.results[0].reserve_id);
                    this.router.navigate(['/frontend/apps/appoint-card']);
                }
                this.showDialog = false;
            }
        } catch (error) {
            console.log(error);
        }
    }

    displayClear() {
        this.customerCID = '';
        this.customerName = '';
        this.customerTel = '';
        this.customerLineId = '';
        this.customerAge = '';
        this.customerChronics = '';
        this.selectHospitals = null;
        this.selectSlot = null;
        this.customerChronicsBtnRadio = '';
    }

    async getCustomerProfile(customerCID: any){
     
        if(customerCID.length == '13'){
            const res: any = await this.lookupService.listCustomerByCID(customerCID);
            
            if(res.data.results.length > 0){
                this.listCustomers = res.data.results[0];

                this.customerName = this.listCustomers.customer_name;
                this.customerTel = this.listCustomers.phone_number;
                this.customerLineId = this.listCustomers.line_id;
                this.customerAge = this.listCustomers.age;
                this.customerCID = this.listCustomers.cid;
                this.customerPassport = this.listCustomers.passport;
                this.customerChronics = this.listCustomers.chronic;
                this.banExpire = this.listCustomers.expire_bane_date;
                this.customerId = this.listCustomers.customer_id;

                if(this.listCustomers.chronic == ""){
                    this.customerChronicsBtnRadio = 'false';
                }else{
                    this.customerChronicsBtnRadio = 'true';
                }
            }
        }
    }

    close(){
        this.showDialog = false;
    }

    // alert message
    showMessages(severity: any, summary: any, detail: any) {
        this.messages = [
            { severity: severity, summary: summary, detail: detail },
        ];
    }
}
