import { RouterModule, Routes } from '@angular/router';
import { NgModule } from '@angular/core';
import { AppLayoutComponent } from './layout/app.layout.component';

const routes: Routes = [
    { path: '', pathMatch: 'full', redirectTo: 'auth/login' },
    {
        path: 'frontend', component: AppLayoutComponent,
        children: [
            { path: '', pathMatch: 'full', redirectTo: 'apps' },
            { path: 'dashboards',data: { breadcrumb: 'Dashboard' }, loadChildren: () => import('./frontend/components/dashboards/dashboards.module').then(m => m.DashboardsModule) },
            { path: 'apps',data: { breadcrumb: 'หน้าหลัก' }, loadChildren: () => import('./frontend/components/apps/apps.module').then(m => m.AppsModule) },
            { path: 'profile', data: { breadcrumb: 'User Management' }, loadChildren: () => import('./frontend/components/profile/profile.module').then(m => m.ProfileModule) },
            { path: 'documentation', data: { breadcrumb: 'Documentation' }, loadChildren: () => import('./frontend/components/documentation/documentation.module').then(m => m.DocumentationModule) },
        ]
    },
    { path: 'auth', data: { breadcrumb: 'Authenticate' }, loadChildren: () => import('./auth/auth.module').then(m => m.AuthModule) },
    { path: 'frontend',data: { breadcrumb: 'หน้าหลัก' }, loadChildren: () => import('./frontend/components/landing/landing.module').then(m => m.LandingModule) },
    { path: 'notfound', loadChildren: () => import('./frontend/components/notfound/notfound.module').then(m => m.NotfoundModule) },
    { path: 'backend', data: { breadcrumb: 'จัดการระบบ' },component: AppLayoutComponent, loadChildren: () => import('./backend/backend.module').then(m => m.BackendModule) },
    { path: 'helper', data: { breadcrumb: 'Helper' }, component: AppLayoutComponent, loadChildren: () => import('./helper/helper.module').then(m => m.HelperModule) },
];
@NgModule({
    imports: [RouterModule.forRoot(routes)],
    exports: [RouterModule]
})
export class AppRoutingModule { }
