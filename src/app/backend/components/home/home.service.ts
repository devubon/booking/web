import { Injectable } from '@angular/core';
import axios from 'axios';
import { environment } from '../../../../environments/environment';

@Injectable({
    providedIn: 'root',
})
export class HomeService {
    // pathPrefix: any = `:40014`
    // pathPrefixAuth: any = `:40001`;
    currentDate: string = new Date().toISOString().substring(0,10);
   
    private axiosInstance = axios.create({
        baseURL: `${environment.apiUrl}`,
    });

    constructor() {
        this.axiosInstance.interceptors.request.use((config) => {
            const token = sessionStorage.getItem('token');
            if (token) {
                config.headers['Authorization'] = `Bearer ${token}`;
            }
            return config;
        });
    }

    async list(hospitalID:any,serviceID:any) {

        let data:any = {
            "reserve_date":this.currentDate,
            "hospital_id":hospitalID,
            "service_id":serviceID
        }

        const url = `/backend/getManagementReserve`;
        return this.axiosInstance.post(url,data);
        
        
    }

    async lookupSlot() {
        const url = `/slots/info`;
        return this.axiosInstance.get(url);
    }

    async getDashboard(hospital_id:any,service_type_id:any) {
        const url = `/backend/countByHospitalID/`+ hospital_id + `/` + service_type_id;
        return this.axiosInstance.get(url);
    }

    async getCustomerById(id:number) {
        const url = `frontend/getByUserIDlimit/`+ id;
        return this.axiosInstance.get(url);
    }

    updateCustomer(id: any, data: any) {
        const url = `/customers/` + id;
        return this.axiosInstance.put(url, data);
    }
}
