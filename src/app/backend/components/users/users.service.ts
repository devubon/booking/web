import { Injectable } from '@angular/core';
import axios from 'axios';
import { environment } from '../../../../environments/environment';

@Injectable({
    providedIn: 'root',
})
export class UsersService {
    // pathPrefix: any = `:40014`
    // pathPrefixAuth: any = `:40001`;

    private axiosInstance = axios.create({
        baseURL: `${environment.apiUrl}`,
    });

    constructor() {
        this.axiosInstance.interceptors.request.use((config) => {
            const token = sessionStorage.getItem('token');
            if (token) {
                config.headers['Authorization'] = `Bearer ${token}`;
            }
            return config;
        });
    }

    async list() {
        const url = `/users`;
        return this.axiosInstance.get(url);
    }

    async getById(id: any) {
        const url = `/users/` + id;
        return this.axiosInstance.get(url);
    }

    async getByHospitalId(id: any) {
        const url = `/users/getByHospitalID/` + id;
        return this.axiosInstance.get(url);
    }

    async save(data: any) {
        const url = `/users`;
        return this.axiosInstance.post(url, data);
    }

    async update(id: any, data: any) {
        const url = `/users/` + id;
        return this.axiosInstance.put(url, data);
    }

    // Profiles

    async listProfiles() {
        const url = `/profiles/info`;
        return this.axiosInstance.get(url);
    }

    async getProfilesById(id: any) {
        const url = `/profiles/getByID/` + id;
        return this.axiosInstance.get(url);
    }

    async saveProfiles(data: any) {
        const url = `/profiles/`;
        return this.axiosInstance.post(url, data);
    }

    async updateProfiles(id: any, data: any) {
        const url = `/profiles/` + id;
        return this.axiosInstance.put(url, data);
    }

    // Roles

    async listRoles() {
        const url = `/roles/info`;
        return this.axiosInstance.get(url);
    }

    async getRolesById(id: any) {
        const url = `/roles/getByID/` + id;
        return this.axiosInstance.get(url);
    }

    async saveRoles(data: any) {
        const url = `/roles`;
        return this.axiosInstance.post(url, data);
    }

    async updateRoles(id: any, data: any) {
        const url = `/roles/` + id;
        return this.axiosInstance.put(url, data);
    }

    async listHospital() {
        const url = `/hospitals/info`;
        return this.axiosInstance.get(url);
    }
    async listHospitalId(id: any) {
        const url = `/hospitals/getByID/${id}`;
        return this.axiosInstance.get(url);
    }
    // Service

    async listService() {
        const url = `/services/info`;
        return this.axiosInstance.get(url);
    }

    async checkUsername(username: any) {
        const url = `checkUser/getByUsername/`+ username;
        return this.axiosInstance.get(url);
    }
}
