import { Injectable } from '@angular/core';
import axios from 'axios';
import { environment } from '../../../environments/environment';

@Injectable({
    providedIn: 'root',
})
export class LookupService {
    // pathPrefix: any = `:40014`
    // pathPrefixAuth: any = `:40001`;

    private axiosInstance = axios.create({
        baseURL: `${environment.apiUrl}`,
    });

    constructor() {
        this.axiosInstance.interceptors.request.use((config) => {
            const token = sessionStorage.getItem('token');
            if (token) {
                config.headers['Authorization'] = `Bearer ${token}`;
            }
            return config;
        });
    }

    async listClinic() {
        const url = `/services/`;
        return this.axiosInstance.get(url);
    }

    async listPeriod() {
        const url = `/periods/info`;
        return this.axiosInstance.get(url);
    }
    async listPeriodid(id:any) {
        const url = `/periods/getByHospitalID/`+id;
        return this.axiosInstance.get(url);
    }
    async listSlot() {
        const url = `/slots/`;
        return this.axiosInstance.get(url);
    }

    async listSlotByID(id:any) {
        const url = `/slots/getByServiceID/`+id;
        return this.axiosInstance.get(url);
    }

    async listHospital() {
        const url = `/hospitals/`;
        return this.axiosInstance.get(url);
    }
    async listHospitalInfo() {
        const url = `/hospitals/info`;
        return this.axiosInstance.get(url);
    }
    

    async listServiceType() {
        const url = `/service_types/`;
        return this.axiosInstance.get(url);
    }
    async listService() {
        const url = `/services/info`;
        return this.axiosInstance.get(url);
    }
    async listRole() {
        const url = `/roles/`;
        return this.axiosInstance.get(url);
    }

    async listUser() {
        const url = `/users/`;
        return this.axiosInstance.get(url);
    }

    async listCustomerByID(id: any) {
        const url = `/customers/getByID/`+id;
        return this.axiosInstance.get(url);
    }

}
